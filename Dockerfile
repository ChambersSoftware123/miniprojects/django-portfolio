# Use an official Python runtime as the base image
FROM python:3.9

# Set the working directory in the container
WORKDIR /app

# Copy the requirements.txt file to the container
COPY requirements.txt .

# Install the dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the Django project code to the container
COPY . .

# Expose the port on which your Django app will run
EXPOSE 80

# Set environment variables if needed
ENV DJANGO_SETTINGS_MODULE=portfolio.settings

# Run the Django development server
CMD ["python", "manage.py", "runserver", "0.0.0.0:80"]